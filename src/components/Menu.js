import React from 'react'
import { Link } from 'react-router-dom'

function Menu() {
  return (
    <div>
        <nav>
            <ul>
                <li><Link to="/"> About</Link></li>
                <li><Link to="/addArticle"> ajouter article</Link></li>
                <li><Link to="/listArticles">liste d'articles</Link></li>
            </ul>
        </nav>
    </div>
  )
}

export default Menu