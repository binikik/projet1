import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addArticle } from '../redux/actionArticles'
function AddArticle(props) {
    const [id,setId]=useState(0)
    const [nom,setNom]=useState("")
    const [prix,setPrix]=useState(0)
    const [quantite,setQuantite]=useState(0)
    const dispatch=useDispatch()
  return (
    <div>AddArticle
        <label>id</label><input type="text" onChange={(ev)=>setId(parseInt(ev.target.value))}/>
        <br/>
        <label>nom</label><input type="text" onChange={(ev)=>setNom(ev.target.value)}/>
        <br/>
        <label>prix</label><input type="text" onChange={(ev)=>setPrix(parseFloat(ev.target.value))}/>
        <br/>
        <label>quantite</label><input type="text"
         onChange={(ev)=>setQuantite(parseInt(ev.target.value))}/><br/>
   <button onClick={()=>dispatch(addArticle(id,nom,prix,quantite))}>Ajouter</button>
    </div>
  )
}
export default AddArticle