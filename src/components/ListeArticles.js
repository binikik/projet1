import React, { useEffect } from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { decrementerQuantite, deleteArticle,fetchArticles,incrementerQuantite } from '../redux/actionArticles'
function ListeArticles() {
    const mesArticles=useSelector(state=>state.articles)
    const session=useSelector(state=>state.user)
    const dispatch=useDispatch()
    useEffect(()=>{
dispatch(fetchArticles())
    },[])
  return (
    <div>
        <h2>session {session.nom}</h2>
        <h3>ListeArticles  nombres articles: {mesArticles.length}</h3>
        <table border="1" className='table table-striped'>
            <thead>
                <tr>
                    <th>id</th><th>nom</th><th>prix</th><th>quantite</th>  
                </tr>
            </thead>
            <tbody>
                {mesArticles.map((art,index)=>{
                    return(<tr key={index}>
                        <td>{art.id}</td>
                        <td>{art.nom}</td>
                        <td>{art.prix}</td>
                        <td>
                            <button className='btn btn-secondary' onClick={()=>dispatch(decrementerQuantite(art.id))}>-</button>
                            {art.quantite} 
                            <button className='btn btn-primary' onClick={()=>dispatch(incrementerQuantite(art.id))}>+</button></td>
                        <td><button className='btn btn-danger' onClick={()=>dispatch(deleteArticle(art.id))}>X</button></td>
                    </tr>)
                })}
            </tbody>
            <button>vider panier</button>
            <h4>Total HT:  300DH</h4>
        </table>
        </div>)
        }
export default ListeArticles