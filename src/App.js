import logo from './logo.svg';
import './App.css';
import ListeArticles from './components/ListeArticles';
import AddArticle from './components/AddArticle';
import 'bootstrap/dist/css/bootstrap.min.css'
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import About from './components/About';
import Menu from './components/Menu';

function App() {
  return (
    <div >
      <BrowserRouter>
      <Menu/>
      <Routes>
        <Route path="/" element={<About/>}/>
        <Route path="/addArticle" element={<AddArticle/>}/>
        <Route path="/listArticles" element={<ListeArticles/>}/>
      </Routes>
      </BrowserRouter>
     
    </div>
  );
}

export default App;
