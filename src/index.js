import React from 'react';
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension';
import { applyMiddleware } from 'redux';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import {legacy_createStore as createStore} from 'redux'
import {Provider} from 'react-redux'
import reducerArticle from './redux/reducerArticles';
const store=createStore(reducerArticle,composeWithDevTools(applyMiddleware(thunk)))

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div>
    <Provider store={store}>
       <App />
    </Provider>
  </div>
);


