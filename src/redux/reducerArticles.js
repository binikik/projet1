import { produce } from "immer"
const initState={articles:[],user:{id:1,nom:"rami"},loading:false,erreur:""}
export default function reducerArticle(state=initState,action){
    switch (action.type) {
        case "FETCH_ARTICLES_REQUEST":
           // return{...state,loading:true}
           return produce(state,(draft)=>{draft.loading=true})
        case "FETCH_ARTICLES_SUCCESS":
            //return{...state,articles:action.payload , loading:false}
            return produce(state,(draft)=>{draft.articles=action.payload;draft.loading=false})
        case "FETCH_ARTICLES_FAILURE":
            //return{...state,erreur:action.payload,articles:[]}
            return produce(state,(draft)=>{draft.erreur=action.payload;draft.articles=[]})
        case "ADD_ARTICLE":
            //return {...state,articles:[...state.articles,action.payload]}
            return produce(state,(draft)=>{draft.articles.push(action.payload)})
        case "VIDER_ARTICLES":
           // return{...state,articles:[]}
           return produce(state,(draft)=>{draft.articles=[]})
        case "DELETE_ARTICLE":
                        //return {...state,articles:state.articles.filter(art=>art.id!==action.payload.id)}
            return produce(state,(draft)=>{draft.articles=draft.articles.filter(art=>art.id!==action.payload.id)})
            case "INCREMENTER_QUANTITE":
//return {...state,articles:state.articles.map(art=>art.id!==parseInt(action.payload.id)?art:{...art,quantite:art.quantite+1})} 
return produce(state,(draft)=>{
    const article=draft.articles.find(art=>art.id===action.payload.id)
    article.quantite+=1
})

case "DECREMENTER_QUANTITE":
    return produce(state,(draft)=>{
const myArticle=draft.articles.find(art=>art.id===action.payload.id)
myArticle.quantite-=1

    })


default:
return state
    }
}