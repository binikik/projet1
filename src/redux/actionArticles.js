import axios from "axios"

export function addArticle(id,nom,prix,quantite){
    return {type:"ADD_ARTICLE",payload:{id:id,nom:nom,prix:prix,quantite:quantite}}
}

export function viderArticles(){
    return {type:"VIDER_ARTICLES"}
}

export function deleteArticle(id){
    return {type:"DELETE_ARTICLE",payload:{id:id}}
}

export function incrementerQuantite(id){
    return {type:"INCREMENTER_QUANTITE",payload:{id:id}}
}

export function decrementerQuantite(id){
    return {type:"DECREMENTER_QUANTITE",payload:{id:id}}
}
//action synchrone pour fetch article
 function fetch_article_request(){
    return{type:"FETCH_ARTICLES_REQUEST"}
    }

function fetch_article_success(articles){
return{type:"FETCH_ARTICLES_SUCCESS",payload:articles}
}

 function fetch_article_failure(err){
    return{type:"FETCH_ARTICLES_FAILURE",payload:err}
    }
//function asynchrone  thunk 
export function fetchArticles(){

    return function(dispatch,getState){
      dispatch(fetch_article_request())
      axios.get("http://localhost:3500/articles")
        .then(resp=>dispatch(fetch_article_success(resp.data)))
        .catch(err=>fetch_article_failure(err))
    }}

